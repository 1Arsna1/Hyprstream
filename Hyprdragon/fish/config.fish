if status is-interactive
    # Commands to run in interactive sessions can go here
  starship init fish | source
  clear
  set fish_greeting " "
  #general
  alias ragnarload='sudo make clean ragnar install'
  alias nv='nvim'
  alias ae='autoexec'
  alias fetch='fastfetch'
  #git
  alias gc='git clone'
  alias gs='git status'
  alias gp='git push'
  alias gpl='git pull'
  alias gst='git stash'
  alias ga='git add'
  alias gall='git add -u'
  alias gb='git branch'
  alias gch='git checkout'
  alias gf='git fetch'
  alias gt='git tag'
  alias gnt='git new tag'
  alias lg='lazygit'
  #file structure
  alias ls='lsd'
  alias l='ls -l'
  alias la='ls -a'
  alias lla='ls -la'
  alias lt='ls --tree'
  alias ..='cd ..'
  alias ...='cd ../..'
  alias .3='cd ../../..'
  alias .4='cd ../../../..'
  alias .5='cd ../../../../..'
  #install and uninstall
  alias flatin='flatpak install'
  alias flatun='flatpak uninstall'
  #update
  alias upgrade='sudo pacman -Syyu && paru -Syyu && flatpak update' #update everything
  alias pacdate='sudo pacman -Syyu' #only update standard pkgs
  alias pardate='paru -Sua --noconfirm' #only update AUR pkgs
  alias unlock='sudo rm /var/lib/pacman/db.lck' #remove pacman lock
  alias cleanup='sudo pacman -Qtdq | sudo pacman -Rns -' #remove orphaned packages
  alias flatup='flatpak update'
  #mirrorlist
  alias mirror='sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist'
  alias mirrord='sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist'
  alias mirrors='sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist'
  alias mirrora='sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist'
  #grep with colors
  alias grep='grep --color=auto'
  alias egrep='egrep --color=auto'
  alias fgrep='fgrep --color=auto'
  #adding flags
  alias cp='cp -i'
  alias df='df -h'
  alias free='freem -m'
  #guess what
  alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
end

function fish_user_key_bindings
  fish_vi_key_bindings
end


function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

function nvidia-settings
    command nvidia-settings --config=$XDG_CONFIG_HOME/nvidia/settings $argv
end


set -x UID (id -u)

set -x XDG_DATA_HOME $HOME/.local/share
set -x XDG_CONFIG_HOME $HOME/.config
set -x XDG_CACHE_HOME $HOME/.cache
set -x XDG_RUNTIME_DIR /run/user/$UID
set -x XDG_DOCUMENTS_DIR $HOME/Documents
set -x XDG_DOWNLOAD_DIR $HOME/Downloads
set -x XDG_MUSIC_DIR $HOME/Music
set -x XDG_PICTURES_DIR $HOME/Pictures
set -x XDG_VIDEOS_DIR $HOME/Videos
set -x XDG_STATE_HOME $HOME/.local/state/

set -x RUNTIME_DIR /run/user/$UID
set -x SHARE_DIR $HOME/.local/share
set -x CONFIG_DIR $HOME/.config
set -x STATE_DIR $HOME/.local/state
set -x CACHE_DIR $HOME/.cache/

#set -x CARGO_HOME $XDG_DATA_HOME/cargo
#set -x GNUPGHOME $XDG_DATA_HOME/gnupg
#set -x GTK2_RC_FILES $XDG_CONFIG_HOME/gtk-2.0/gtkrc
#set -x NUGET_PACKAGES $XDG_CACHE_HOME/NuGetPackages
#set -x RUSTUP_HOME $XDG_DATA_HOME/rustup
#set -x WINEPREFIX $XDG_DATA_HOME/wine
#set -x CUDA_CACHE_PATH $XDG_DATA_HOME/nv
#set -x NPM_CONFIG_USERCONFIG $XDG_CONFIG_HOME/npm/npmrc
#set -x XINITRC "$XDG_CONFIG_HOME"/X11/xinitrc
#set -x XAUTHORITY "$XDG_RUNTIME_DIR"/Xauthority
set -x CARGO_HOME "$XDG_DATA_HOME"/cargo
set -x GNUPGHOME "$XDG_DATA_HOME"/gnupg
set -x GTK2_RC_FILES "$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
set -x NUGET_PACKAGES "$XDG_CACHE_HOME"/NuGetPackages
set -x RUSTUP_HOME "$XDG_DATA_HOME"/rustup
set -x WINEPREFIX "$XDG_DATA_HOME"/wine
set -x CUDA_CACHE_PATH "$XDG_DATA_HOME"/nv
set -x NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME"/npm/npmrc
set -x XINITRC "$XDG_CONFIG_HOME"/X11/xinitrc
set -x XAUTHORITY "$XDG_CONFIG_HOME"/X11/Xauthority


# Created by `pipx` on 2023-05-26 12:17:10
set PATH $PATH /home/soul/.local/bin

# HyprStream

**base dependencies:**

> make gdb ninja gcc cmake meson libxcb xcb-proto xcb-util xcb-util-keysyms libxfixes libx11 libxcomposite xorg-xinput libxrender pixman wayland-protocols cairo pango seatd libxkbcommon xcb-util-wm xorg-xwayland libinput libliftoff libdisplay-info wayland wlroots pacman-contrib

**system dependencies:**

> networkmanager network-manager-applet pipewire pipewire-pulse pipewire-jack wireplumber pipewire-alsa lib32-libpipewire lib32-pipewire lib32-pipewire-jack easyeffects pavucontrol grim slurp cliphist wl-clipboard ttf-hack-nerd ttf-nerd-fonts-symbols ttf-nerd-fonts-symbols-common ttf-nerd-fonts-symbols-mono mpd ffmpeg mpv mpv-mpris playerctl libmpd jq qt5-base qt5-wayland qt5ct qt6-base qt6-wayland qt6ct electron kvantum gtk-engine-murrine gtk-engines xdg-desktop-portal-hyprland mate-polkit ttf-tinos-nerd 

**aur**
xwaylandvideobridge-cursor-mode-2-git mpd-mpris mpdris2 wlr-randr waybar-module-pacman-updates-git

**ricing applications:**

> alacritty dunst starship fish btop lsd neovim geany rofi-lboon-wayland-git bat nemo 

**aur**
>swayosd-git nwg-look-bin wlogout waybar-hyprland-git cava pipes.sh tty-clock swww hyprpicker-git swaylock-effects-git

**How to install:**

> Backup your own dotfiles, clone this repo and move all folders into .config except Pictures and .scripts, they are supposed to be in your home directory

**Issues:**
> If the Wallpaper does not want to appear/change change your monitor in hyprland.conf

